package com.sellics.amazon.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author Shankar Karumalai
 * @since 20 Mar, 2022
 */
@Service
public class AmazonClientService {

    private static AmazonS3 amazonS3;

    private static AmazonS3Properties amazonS3Properties;

    @Autowired
    private AmazonClientService(AmazonS3Properties amazonS3Properties) {
        this.amazonS3Properties = amazonS3Properties;
    }

    @PostConstruct
    private static void init() {

        BasicAWSCredentials credentials = new BasicAWSCredentials(amazonS3Properties.getAccessKey(), amazonS3Properties.getSecretKey());

       amazonS3 = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.fromName(amazonS3Properties.getRegion()))
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    /**
     * We can cache the data in case the data is not updated frequently and evict the cache when there is a change in the data.
     * @return S3Object
     */
    public static S3Object  getRankingData() {
        if(amazonS3 == null) init();
        return amazonS3.getObject(new GetObjectRequest(amazonS3Properties.getBucketName(), amazonS3Properties.getKey()));
    }

}