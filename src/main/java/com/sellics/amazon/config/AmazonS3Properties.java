package com.sellics.amazon.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Shankar Karumalai
 * @since 20 Mar, 2022
 */
@Data
@Component
@ConfigurationProperties("amazon.s3")
public class AmazonS3Properties {

    private String bucketName;
    private String key;
    private String accessKey;
    private String secretKey;
    private String region;
}
