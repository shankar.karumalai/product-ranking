package com.sellics.amazon.controller;

import com.sellics.amazon.exception.NotFoundException;
import com.sellics.amazon.model.IndividualProductRankingDTO;
import com.sellics.amazon.service.ProductRankingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author Shankar Karumalai
 * @since 20 Mar, 2022
 */
@RestController
@RequestMapping(path = "/api/v1/product/ranking")
public class ProductRankingController {

    ProductRankingService productRankingService;

    @Autowired
    public ProductRankingController(ProductRankingService productRankingService) {
        this.productRankingService = productRankingService;
    }

    @GetMapping("/individual")
    public ResponseEntity<List<IndividualProductRankingDTO>> getIndividualRankByKeyword(@RequestParam String keyword) throws NotFoundException {
        var result = productRankingService.getIndividualRankByKeyword(keyword);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/aggregate/keyword")
    public ResponseEntity<Map<Long, Map<String, Integer>>> getAggregateRankByKeyword(@RequestParam String keyword) throws NotFoundException {
        var result = productRankingService.getAggregateRankByKeyword(keyword);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/aggregate/asin")
    public ResponseEntity<Map<Long, Map<String, Integer>>> getAggregateRankByAsin(@RequestParam String asin) throws NotFoundException {
        var result = productRankingService.getAggregateRankByAsin(asin);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleNotFoundExceptionException(NotFoundException e) {
        HttpStatus httpStatus = e.getHttpStatus();
        if (null == e.getHttpStatus()) {
            httpStatus = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(e.getMessage(), httpStatus);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleException(Exception e) {
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        return new ResponseEntity<>(e.getMessage(), httpStatus);
    }

}
