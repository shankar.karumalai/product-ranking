package com.sellics.amazon.service;

import com.amazonaws.services.s3.model.S3Object;
import com.opencsv.bean.CsvToBeanBuilder;
import com.sellics.amazon.config.AmazonClientService;
import com.sellics.amazon.exception.NotFoundException;
import com.sellics.amazon.model.AggregateAsinDTO;
import com.sellics.amazon.model.AggregateKeywordDTO;
import com.sellics.amazon.model.IndividualProductRankingDTO;
import com.sellics.amazon.model.ProductRanking;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

/**
 * @author Shankar Karumalai
 * @since 20 Mar, 2022
 */

@Service
public class ProductRankingService {

    private static final int CSV_KEYWORD_COLUMN_NUMBER = 2;
    private static final int CSV_ASIN_COLUMN_NUMBER = 1;
    private static final String KEYWORD_NOT_FOUND = "Keyword not found!";
    private static final String ASIN_NOT_FOUND = "ASIN not found!";


    public List<IndividualProductRankingDTO> getIndividualRankByKeyword(String keyword) throws NotFoundException {
        List<ProductRanking>  productRankings = getAllRankByFilter(keyword, CSV_KEYWORD_COLUMN_NUMBER);

        validateData(CollectionUtils.isEmpty(productRankings), KEYWORD_NOT_FOUND);

        return productRankings.stream().map(this::castProductRankDataToIndividualRank).collect(Collectors.toList());
    }

    public Map<Long, Map<String, Integer>> getAggregateRankByKeyword(String keyword) throws NotFoundException {
        List<ProductRanking> productRankings = getAllRankByFilter(keyword, CSV_KEYWORD_COLUMN_NUMBER);

        validateData(CollectionUtils.isEmpty(productRankings), KEYWORD_NOT_FOUND);

        return computeAggregateProductRankingByKeyword(productRankings);
    }

    public Map<Long, Map<String, Integer>> getAggregateRankByAsin(String asin) throws NotFoundException {
        List<ProductRanking> productRankings = getAllRankByFilter(asin, CSV_ASIN_COLUMN_NUMBER);

        validateData(CollectionUtils.isEmpty(productRankings), ASIN_NOT_FOUND);

        return computeAggregateProductRankingByAsin(productRankings);
    }

    private Map<Long, List<AggregateAsinDTO>> getProductRankingByKeyword(List<ProductRanking> productRankings) {
        Map<Long, List<AggregateAsinDTO>> rankingByKeywordMap = new HashMap<>();
        productRankings.stream().forEach(productRanking -> constructAggregateResultByKeyword(rankingByKeywordMap, productRanking));
        return rankingByKeywordMap;
    }

    private Map<Long, List<AggregateKeywordDTO>> getProductRankingByAsin(List<ProductRanking> productRankings) {
        Map<Long, List<AggregateKeywordDTO>> rankingByAsinMap = new HashMap<>();
        productRankings.stream().forEach(productRanking -> constructAggregateResultByAsin(rankingByAsinMap, productRanking));
        return rankingByAsinMap;
    }

    private Map<Long, Map<String, Integer>> computeAggregateProductRankingByKeyword(List<ProductRanking> productRankings) {
        Map<Long, Map<String, Integer>> result = new HashMap<>();
        Map<Long, List<AggregateAsinDTO>> productRankingByTimestamp = getProductRankingByKeyword(productRankings);
        productRankingByTimestamp.forEach((key, value) -> {
            Map<String, Integer> aggregateValues = value.stream()
                    .filter(productRank -> (productRank != null && productRank.getRank() != null && productRank.getAsin() != null))
                    .collect(groupingBy(AggregateAsinDTO::getAsin, summingInt(AggregateAsinDTO::getRank)));
            result.put(key, aggregateValues);
        });
        return result;
    }

    private Map<Long, Map<String, Integer>> computeAggregateProductRankingByAsin(List<ProductRanking> productRankings) {
        Map<Long, Map<String, Integer>> result = new HashMap<>();
        Map<Long, List<AggregateKeywordDTO>> productRankingByTimestamp = getProductRankingByAsin(productRankings);
        productRankingByTimestamp.forEach((key, value) -> {
            Map<String, Integer> aggregateValues = value.stream()
                    .filter(productRank -> (productRank != null && productRank.getKeyword() != null && productRank.getRank() != null))
                    .collect(groupingBy(AggregateKeywordDTO::getKeyword, summingInt(AggregateKeywordDTO::getRank)));
            result.put(key, aggregateValues);
        });
        return result;
    }

    private void constructAggregateResultByKeyword(Map<Long, List<AggregateAsinDTO>> rankingByAsinMap, ProductRanking productRanking) {
        List<AggregateAsinDTO> productRankingList
                = rankingByAsinMap.getOrDefault(productRanking.getTimestamp(), new ArrayList<>());
        productRankingList.add(AggregateAsinDTO.builder().rank(productRanking.getRank()).asin(productRanking.getAsin()).build());
        rankingByAsinMap.put(productRanking.getTimestamp(), productRankingList);
    }

    private void constructAggregateResultByAsin(Map<Long, List<AggregateKeywordDTO>> rankingByAsinMap, ProductRanking productRanking) {
        List<AggregateKeywordDTO> productRankingList
                = rankingByAsinMap.getOrDefault(productRanking.getTimestamp(), new ArrayList<>());
        productRankingList.add(AggregateKeywordDTO.builder().rank(productRanking.getRank()).keyword(productRanking.getKeyword()).build());
        rankingByAsinMap.put(productRanking.getTimestamp(), productRankingList);
    }

    private IndividualProductRankingDTO castProductRankDataToIndividualRank(ProductRanking productRanking) {
        IndividualProductRankingDTO individualProductRankingResponse = new IndividualProductRankingDTO();
        BeanUtils.copyProperties(productRanking, individualProductRankingResponse);
        return individualProductRankingResponse;
    }

    private List<ProductRanking> getAllRankByFilter(String fiterKey, int colmunNumber) {
        S3Object s3Object = AmazonClientService.getRankingData();
        return new CsvToBeanBuilder<ProductRanking>(new InputStreamReader(s3Object.getObjectContent()))
                .withType(ProductRanking.class)
                .withSeparator(';')
                .withFilter(line -> line[colmunNumber].equals(fiterKey))
                .build().parse();
    }

    private void validateData(boolean isEmptyData, String message) throws NotFoundException {
        if (isEmptyData) throw new NotFoundException(message, HttpStatus.NOT_FOUND);
    }

}
