package com.sellics.amazon.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

/**
 * @author Shankar Karumalai
 * @since 21 Mar, 2022
 */
@Data
public class ProductRanking {

    @CsvBindByPosition(position = 0)
    private Long timestamp;

    @CsvBindByPosition(position = 1)
    private String asin;

    @CsvBindByPosition(position = 2)
    private String keyword;

    @CsvBindByPosition(position = 3)
    private Integer rank;
}
