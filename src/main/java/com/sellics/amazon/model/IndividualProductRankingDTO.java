package com.sellics.amazon.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Shankar Karumalai
 * @since 21 Mar, 2022
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IndividualProductRankingDTO {

    private Long timestamp;
    private String asin;
    private Integer rank;
}
