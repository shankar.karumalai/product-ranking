package com.sellics.amazon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductRankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductRankingApplication.class, args);
	}

}
