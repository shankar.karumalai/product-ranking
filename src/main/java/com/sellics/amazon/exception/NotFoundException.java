package com.sellics.amazon.exception;

import org.springframework.http.HttpStatus;

/**
 * @author Shankar Karumalai
 * @since 20 Mar, 2022
 */
public class NotFoundException extends Exception{

    private final String message;
    private final HttpStatus httpStatus;

    public NotFoundException(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
