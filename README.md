# Product ranking

Sellics SaaS products aim to improve the efficiency of, and facilitate a better outcome for retailers using Amazon as a platform to sell their products.

## Solutions

- [x] opencsv
    - Opencsv is an easy-to-use CSV (comma-separated values) parser library for Java. It was developed because all the CSV parsers at the time didn’t have commercial-friendly licenses. Java 8 is currently the minimum supported version.
        - Implementation
            -  Apply the filter with support of opencsv and read the output data
            - Compute the data based on our business logic

- [ ] csvjdbc
    - [csvjdbc](http://csvjdbc.sourceforge.net/doc.html) is another powerful tool we can implement the solution even with much lesser code with sql queries.

- [ ] Secondary Cache
    - We can cache the data into the Redis or any other secondary cache for better manipulation.


### Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

```
git clone https://gitlab.com/shankar.karumalai/product-ranking.git
cd product-ranking
mvn clean install
```

#### [Build and run your application](https://docs.spring.io/spring-boot/docs/1.5.16.RELEASE/reference/html/using-boot-running-your-application.html)

### Postman
[Postman Collections](https://www.getpostman.com/collections/a924135f1b878a5081ab) are a group of saved requests. Every request you send in Postman appears under the History tab of the sidebar.
On a small scale, reusing requests through the history section is convenient.

#### [Swagger : API Documentation](https://product-ranking.herokuapp.com/swagger-ui/index.html)
Simplify API development for users, teams, and enterprises with our open source and professional toolset.
![swagger](swagger.png)

